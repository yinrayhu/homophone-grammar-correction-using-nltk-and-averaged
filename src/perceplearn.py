# !/usr/bin/python3

"""
get class names & feature space from input line
average perceptron
output MODELFILE:
CLASS FEATURE WEIGHT
"""

import os
import sys
import random

# "label" is a list of strings for class names
# label_weight dict records current weight for each label
# avg_w is the averaged weight vector to write to MODELFILE
# current_result is a dict {'label_name': classify_product}

MAX_ITER = 8
#MAX_ITER = 16

def disp_usage():
    print('\nERROR: Incorrect number of arguments or file not found.')
    print('USAGE: python3 perceplearn.py TRAININGFILE MODELFILE\n')


def read_file(infile):
    input_as_list = [] # for convenience of shuffling
    label_list = []
    feat_space = []
    print('\nReading input file. Adding class labels and feature space.\n')
    with open(infile, 'r') as f:
        for line in f:
            label_list = add_label(line.split()[0], label_list)
            feat_space = add_dim(line.split()[1:], feat_space)
            input_as_list.append(line)
    return input_as_list, label_list, feat_space

    
def add_label(label, label_list):
    if label not in label_list:
        label_list.append(label)
    return label_list


def add_dim(feat_vec, feat_space):
    # increase feature space dimension if encounter unseen feature
    for feat in feat_vec:
        if feat not in feat_space: feat_space.append(feat)
    return feat_space


class LearnClass:
    # inline is a line from input file
    # self.label is the correct class label from training, y
    # w is the weight vector, dict of weights
    # weights are numbers, keys of dict w are words (features), x
    
    # access LearnClass using a dict of class objects
    # e.g., initialize --> add a class object to dict
    # by mydict[label] = LearnClass()
    # add training samples and update w --> mydict[label].update(inline)
    
    def __init__(self, name, feat_space):
        # initialize w by setting all weights to zero
        self.w = {}
        self.name = name
        for feat in feat_space:
            self.w[feat] = 0

    def input(self, inline):
        self.label, self.x = inline.split()[0], inline.split()[1:]

    def inner_product(self, inline):
        # return classification result, compare to other,
        # come back to update if needed
        self.input(inline)
        self.z = 0
        for item in self.x:
            #print('WORD:',item,'  WEIGHT:',self.w[item])
            self.z += self.w[item]
        return self.z

    def increase(self, inline):
        self.input(inline)
        for item in self.x:
            self.w[item] += 1
    def decrease(self, inline):
        self.input(inline)
        for item in self.x:
            self.w[item] -= 1
    
    def print_w(self):
        print('\nWeight vector:', self.w, '\n')
    def print_name(self):
        print('\nThe label of this class is:', self.name, '\n')

def iterate(input_as_list, label_list, feat_space, MAX_ITER):
    # initialize class object dictionary using labels and feature space
    # initialize {'label': {w_sum} } dict of dict 
    classdict = {label: LearnClass(label, feat_space) for label in label_list}
    # see ML_textbook_draft.pdf, p. 13 for usage of cached_weight_u
    cached_weight_u = {label: {} for label in label_list}
    for label in cached_weight_u.keys():
        cached_weight_u[label] = {feat:0 for feat in feat_space}
    c = 1
    # initiaze avg_w; later will calculate avg_w = classdict - 1/c * cached_weight_u
    avg_w = {label: {} for label in label_list}
    for label in avg_w.keys():
        avg_w[label] = {feat:0 for feat in feat_space}
        
    for iter in range(MAX_ITER):
        # 1 iteration for averaged perceptron, shuffle data samples first
        print('Iteration no.', str(iter+1))
        random.shuffle(input_as_list)
        for line in input_as_list:
            #print('\n',line)
            correct = line.split()[0]
            current_result = {}
            for label in label_list:
                current_result[label] = classdict[label].inner_product(line)
            guess = pick_class(label_list, current_result)
            
            if guess != correct:
                # the "guess" class should be penalized, "correct" class rewarded
                classdict[guess].decrease(line)
                classdict[correct].increase(line)
                for feat in line.split()[1:]:
                    # penalize by subtraction
                    cached_weight_u[guess][feat] -= c
                    # reward by addition
                    cached_weight_u[correct][feat] += c             
            c += 1

    for label in avg_w.keys():
        # get averaged weight vector for each class
        for feat in avg_w[label].keys():
            avg_w[label][feat] = (classdict[label].w[feat] -
                                  1/c * cached_weight_u[label][feat])       
    return avg_w


def pick_class(label_list, current_result):
    # current_result = {'label_name': inner_product, ...}
    # pick classification result using argmax_i{z_i}

    # pick largest product from classification result,
    # start from 0th label and continue comparison
    argmax = label_list[0]
    for label in label_list[1:]:
        if current_result.get(label) > current_result.get(argmax):
            argmax = label
    return argmax


def write_model(avg_w, modelfile):
    # format in MODELFILE:    CLASS    FEAT    WEIGHT
    with open(modelfile, 'w') as f:
        for label in avg_w.keys():
            for feat in avg_w[label].keys():
                if avg_w[label][feat] != 0:
                    f.write(label+' '+feat+' '+str(avg_w[label][feat])+'\n')
    print('\nModel successfully written to', modelfile, '\n')

    
def main():
    try:
        INFILE = sys.argv[1]
        MODELFILE = sys.argv[2]
        input_as_list, label_list, feat_space = read_file(INFILE)
    except IndexError:
        disp_usage()
        return
    except OSError:
        disp_usage()
        return

    avg_w = iterate(input_as_list, label_list, feat_space, MAX_ITER)           
    # print(avg_w)
    write_model(avg_w, MODELFILE)


if __name__ == '__main__':
    main()
