# !/usr/bin/python3
""" hw3 step 1: preprocessing to get training data from web text """
# it's vs its ---> IT case
# you're vs your ---> YOU case
# they're vs their ---> THEY case
# loose vs lose ---> LOS case
# to vs too ---> TO case
# 
# 1. Make every character lower case. 
# 2. Change target words with apostrophe into separated by "_". E.g. "they're" to "they_re". 
#    it's -> it_s; you're -> you_re   
# 3. Take POS as a class variable to share across instances b.c. one sentence might contain 
#    multiple homophones

import sys
import nltk
import os


def show_usage():
    print('\nUSAGE: python3 preprocess_noloc.py <raw_text_data_file>')
    print('e.g. python3 preprocess_noloc.py ../../data/one_mil_sentences.dat\n')
    return


def check_training_files():
    # create training file from hw3.dev.txt
    IT = '../../training/IT.train.noloc'
    YOU = '../../training/YOU.train.noloc'
    THEY = '../../training/THEY.train.noloc'
    LOS = '../../training/LOS.train.noloc'
    TO = '../../training/TO.train.noloc'
    if os.path.isfile(IT) or os.path.isfile(YOU) or os.path.isfile(THEY) or os.path.isfile(LOS) or os.path.isfile(TO):
        print('\n[ERROR]: Training files exist! Do you want to continue?\n')
        return True
    else:
        return False


class Homophone():
    """ a class object to create features and write to training file for each homophone case """
    # POS as a class variable to share across Homophone instances
    pos = []
    def __init__(self, homophone1, homophone2):
        # feat is individual word, POS, location; feat_vec is a feature vector
        # 5 feats in a feat_vec: LL_POS, L_POS, R_POS, RR_POS, LL_w, L_w, R_w, RR_w, LOC
        # samples is a list of training samples, write each sample to a line in training file
        self.feat_vec = [''] * 9
        self.sample_list = []
        self.word1 = homophone1
        self.word2 = homophone2

    def get_feat(self):
        """ use POS list of tuples to create features """
        # go through POS tagged list of tuples, each tuple is a ('word', 'pos') pair
        for i, pair in enumerate(Homophone.pos):
            self.feat_vec = []
            if pair[0] == self.word1:
                self.create_feat(i, pair)
                self.sample_list.append(self.word1 + ' ' + ' '.join(self.feat_vec))
                continue
            if pair[0] == self.word2:
                self.create_feat(i, pair)
                self.sample_list.append(self.word2 + ' ' + ' '.join(self.feat_vec))
        return

    def create_feat(self, i, pair):
        """ given index of the desired (word, pos) pair, construct feat_vec"""
        # LOC feature is the relative location of the target word in a sentence
        lword, lpos = Homophone.pos[i-1]
        rword, rpos = Homophone.pos[i+1]
        llword, llpos = Homophone.pos[i-2]
        rrword, rrpos = Homophone.pos[i+2]
        #LOC = 'LOC_' + str(int(10 * i / len(Homophone.pos)))
        #self.feat_vec = [LOC, 'LLW_'+llword, 'LW_'+lword, 'RW_'+rword, 'RRW_'+rrword, 'LLP_'+llpos, 'LP_'+lpos, 'RP_'+rpos, 'RRP_'+rrpos]
        self.feat_vec = ['LLW_'+llword, 'LW_'+lword, 'RW_'+rword, 'RRW_'+rrword, 'LLP_'+llpos, 'LP_'+lpos, 'RP_'+rpos, 'RRP_'+rrpos]
        return

    def postag(line):
        """ use NLTK to obtain POS tagged list of tuples """
        Homophone.pos = [('BOS','BOS'), ('BOS','BOS')]
        tokens = nltk.word_tokenize(line)
        tagged = nltk.pos_tag(tokens)
        Homophone.pos += tagged
        Homophone.pos += [('EOS','EOS'), ('EOS','EOS')]
        return

    def write_file(self, myfile):
        with open(myfile, 'w') as f:
            for sample in self.sample_list:
                f.write(sample + '\n')
        return


def underscore(line):
    """ replace "'s" with underscore to avoid separation """
    inlist = line.split()
    outlist = [''] * len(inlist)
    for i, word in enumerate(inlist):
        if word == "it's": outlist[i] = "it_s"
        elif word == "you're": outlist[i] = "you_re"
        elif word == "they're": outlist[i] = "they_re"
        else: outlist[i] = word
    outline = ' '.join(outlist)
    return outline


def main():
    if check_training_files(): return
    try:
        print('\nPreprocess:', sys.argv[1])
        with open(sys.argv[1], 'r') as f:
            IT = Homophone("its", "it_s")
            YOU = Homophone("your", "you_re")
            THEY = Homophone("their", "they_re")
            LOS = Homophone("lose", "loose")
            TO = Homophone("to", "too")

            linecount = 0
            for line in f:
                linecount += 1
                print('Preprocessing line number:', linecount)
                line = line.lower()
                if "it's" or "you're" or "they're" in line:
                    line = underscore(line)

                # check for homophones, first set POS to its initial state
                # when encounter a homophone, check if the senetence is already POS tagged
                Homophone.pos = []
                if 'its' or "it_s" in line:
                    if not Homophone.pos: Homophone.postag(line)
                    IT.get_feat()

                if 'your' or "you_re" in line:
                    if not Homophone.pos: Homophone.postag(line)
                    YOU.get_feat()

                if 'their' or "they_re" in line:
                    if not Homophone.pos: Homophone.postag(line)                    
                    THEY.get_feat()

                if 'lose' or 'loose' in line:
                    if not Homophone.pos: Homophone.postag(line)
                    LOS.get_feat()

                if 'to' or 'too' in line: 
                    if not Homophone.pos: Homophone.postag()
                    TO.get_feat()
            print('\nTotal number of lines is:', linecount)

    except IndexError:
        print('\n[ERROR]: Wrong number of input arguments!')
        show_usage()
        return
    except OSError:
        print('\n[ERROR]: Cannot find file:', sys.argv[1])
        show_usage()
        return

    print('\nWriting training files.')
    IT.write_file('../../training/IT.train.noloc')
    YOU.write_file('../../training/YOU.train.noloc')
    THEY.write_file('../../training/THEY.train.noloc')
    LOS.write_file('../../training/LOS.train.noloc')
    TO.write_file('../../training/TO.train.noloc')
    print('\nDone.\n')


if __name__ == '__main__': 
    main()