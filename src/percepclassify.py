# !/usr/bin/python3
"""Classification using multiclass perceptron model:""" 
##-- Use a similar format for input and output as nbclassify, 
##-- but instead of taking an input file in the command line, 
##-- take its input from STDIN. 
##
##-- Output should be written to STDOUT 
##-- immediately after each corresponding line is entered via STDIN. 
##-- Make sure to flush STDOUT after writing each line of output. 
##-- i.e. print( RESULT, file=sys.stdout, flush=True) 
##-- **this is not a concern for Python3**

import sys


def disp_usage():
    """display usage of percepclassify.py"""
    print('\nWARNING: FILE I/O FORMAT ERROR OR FILE NOT FOUND', file=sys.stderr)
    print('USAGE[1]: python3 percepclassify.py MODELFILE', file=sys.stderr)
    print('Then input test samples in STDIN, one sample at a time.', file=sys.stderr)
    print('USAGE[2]: python3 percepclassify.py MODELFILE < STDIN_file > STDOUT_file\n', file=sys.stderr)
    return


def get_weight(MODELFILE):
    """get MODEL weight w_avg from MODELFILE, which is tranined using averaged perceptron algorithm"""
    label_list = []
    feat_space = []
    w_avg = {}
    count = 0
    print('\nReading MODELFILE. Getting class labels and feature weights.\n', file=sys.stderr)
    with open(MODELFILE, 'r') as f:
        for line in f:
            # format of MODELFILE:  [ class_label,  feat,  weight ]
            # transform to w_avg[class_label][feat] = weight
            [label, feat, weight] = line.split()
            if label not in w_avg.keys():
                w_avg[label] = {}
                label_list.append(label)
            else:
                w_avg[label][feat] = weight
            if feat not in feat_space:
                count += 1
                # print('Reading feature number', count, file=sys.stderr)
                feat_space.append(feat)
    print('\nModel weights w_avg extracted.\n', file=sys.stderr)
    # print(w_avg)
    return label_list, feat_space, w_avg


def classify(x, w_avg, label_list):
    """classify test sample x using multiclass averaged perceptron weight w_avg"""
    # x is the test sample, z is classification output,
    # current_results store inner products of all classes
    # print("label list in function 'classify':", label_list)
    current_results = {}
    for label in label_list:
        current_results[label] = 0
        for feat in x.split():
            if feat in w_avg[label].keys():
                current_results[label] += float(w_avg[label][feat])
    z = pick_class(label_list, current_results)
    return z


def pick_class(label_list, current_result):
    """pick the label with the largest inner product z from multiple classes"""
    # current_result = {'label_name': inner_product, ...}
    # pick classification result using argmax_i{z_i}

    # pick largest product from classification result,
    # start from 0th label and continue comparison
    argmax = label_list[0]
    for label in label_list[1:]:
        if current_result.get(label) > current_result.get(argmax):
            argmax = label
    return argmax


def main():
    try:
        label_list, feat_space, w_avg = get_weight(sys.argv[1])
        # input_as_list, label_list, feat_space = read_file(INFILE)
        # print('read label list 1:', label_list)
    except IndexError:
        disp_usage()
        return
    except OSError:
        disp_usage()
        return

    test_sample, result = [], []
    while test_sample != '':
        try:
            test_sample = input()
            result = classify(test_sample, w_avg, label_list)
            print(result)
        except ValueError:
            print('Input value error.', file=sys.stderr)
            return
        except EOFError:
            print('End of input file.', file=sys.stderr)
            return

if __name__ == '__main__':
    main()
