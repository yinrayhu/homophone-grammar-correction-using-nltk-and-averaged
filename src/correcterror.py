# !/usr/bin/python3
""" Homophone word correction on test data """
# 1. Similar to percepclassify.py, load five model files. 
# 2. Create a copy of test text. Preserve it for write back.
# 3. Similar to preprocess.py, make test text lower case and scan for homophone cases.
# 4. Classify when encounters any of the five cases, using the corresponding model
# 5. Write back to original test text. Preserve all original format (capital letter, punctuation, etc.)

import sys
import os
import nltk
import string


def show_usage():
    """display usage of correcterror.py"""
    print('\nWARNING: FILE I/O FORMAT ERROR OR FILE NOT FOUND!', file=sys.stderr)
    print('USAGE: python3 correcterror.py IN_file', file=sys.stderr)
    print('RESULT: Output will be printed to STDOUT \n', file=sys.stderr)
    return


def apostrophe(word):
    if '_' in word:
        word = word.replace('_', "'")
    return word


def underscore(line):
    """ replace "'s" with underscore to avoid separation """
    inlist = line.split()
    outlist = [''] * len(inlist)
    for i, word in enumerate(inlist):
        if word == "it's": outlist[i] = "it_s"
        elif word == "you're": outlist[i] = "you_re"
        elif word == "they're": outlist[i] = "they_re"
        else: outlist[i] = word
    outline = ' '.join(outlist)
    return outline


def check_capital(original, classified):
    """ check if original test word capitalized or not, modify classified result accordingly. """
    if original.isupper():
        classified = classified.upper()
    elif original[0].isupper():
        classified = classified.capitalize()
    return classified


def map_back(inline, target_location, corrections):
    """ use target_location and corrections list to modify test_sample_line """
    mylist = inline.split()
    for i, word in enumerate(mylist):
        #print('i =',i, file=sys.stderr)
        #print('target_location =',target_location, file=sys.stderr)
        if not target_location: 
            break
        if i == target_location[0]:
            mylist[i] = check_capital(mylist[i], corrections[0])
            del(target_location[0])
            del(corrections[0])
    result_line = ' '.join(mylist)
    return result_line


# this CANNOT acheive desired result!
# def detokenize(tokens, sample_line):
#     """ given tokens and original line, join tokens and match capitalization of each word """
#     # detokenization line from stackoverflow.com
#     # then split lines, compare word by word.
#     templine = "".join([" "+i if not i.startswith("'") and i not in string.punctuation else i for i in tokens]).strip()
#     print('ORIGINAL', sample_line, file=sys.stderr)
#     print('CLASSIFY:', templine, file=sys.stderr)
#     tempsplit = templine.split()
#     samplesplit = sample_line.split()
#     for i, word in enumerate(tempsplit):
#         tempsplit[i] = check_capital(samplesplit[i], word)
#     outline = ' '.join(tempsplit)
#     return outline

# def write_back_line(line, loc, word):
#     """ return a modified line given result_word, location, original line """
#     word = apostrophe(word)
#     word = check_capital(line[loc], word)
#     line[loc] = word
#     return line


def postag(tokens):
    """ use NLTK to obtain POS tagged list of tuples for input line """
    pos = [('BOS','BOS'), ('BOS','BOS')]
    tagged = nltk.pos_tag(tokens)
    pos += tagged
    pos += [('EOS','EOS'), ('EOS','EOS')]
    return pos


def create_feat(location, pos):
    """ given pos tagged senetence and desired word location, return a feature vector """
    # location in original token is different from POS, which is EOS, BOS padded!
    location += 2
    lword, lpos = pos[location-1]
    rword, rpos = pos[location+1]
    llword, llpos = pos[location-2]
    rrword, rrpos = pos[location+2]
    #LOC = 'LOC_' + str(int(10 * location / len(pos)))
    # originally LOC is feat_vec[0]
    feat_vec = ['LLW_'+llword, 'LW_'+lword, 'RW_'+rword, 'RRW_'+rrword, 'LLP_'+llpos, 'LP_'+lpos, 'RP_'+rpos, 'RRP_'+rrpos]
    feat_vec = ' '.join(feat_vec)
    return feat_vec


""" The following methods come from percepclassify.py """
def get_weight(MODELFILE):
    """get MODEL weight w_avg from MODELFILE, which is tranined using averaged perceptron algorithm"""
    label_list = []
    feat_space = []
    w_avg = {}
    count = 0
    print('\nReading MODELFILE. Getting class labels and feature weights.\n', file=sys.stderr)
    with open(MODELFILE, 'r') as f:
        for line in f:
            # format of MODELFILE:  [ class_label,  feat,  weight ]
            # transform to w_avg[class_label][feat] = weight
            [label, feat, weight] = line.split()
            if label not in w_avg.keys():
                w_avg[label] = {}
                label_list.append(label)
            else:
                w_avg[label][feat] = weight
            if feat not in feat_space:
                count += 1
                # print('Reading feature number', count, file=sys.stderr)
                feat_space.append(feat)
    print('\nModel weights w_avg extracted.\n', file=sys.stderr)
    # print(w_avg)
    return label_list, feat_space, w_avg


def classify(x, w_avg, label_list):
    """classify test sample x using multiclass averaged perceptron weight w_avg"""
    # x is the test sample, z is classification output,
    # current_results store inner products of all classes
    # print("label list in function 'classify':", label_list)
    current_results = {}
    for label in label_list:
        current_results[label] = 0
        for feat in x.split():
            if feat in w_avg[label].keys():
                current_results[label] += float(w_avg[label][feat])
    z = pick_class(label_list, current_results)
    return z


def pick_class(label_list, current_result):
    """pick the label with the largest inner product z from multiple classes"""
    # current_result = {'label_name': inner_product, ...}
    # pick classification result using argmax_i{z_i}

    # pick largest product from classification result,
    # start from 0th label and continue comparison
    argmax = label_list[0]
    for label in label_list[1:]:
        if current_result.get(label) > current_result.get(argmax):
            argmax = label
    return argmax


def main():
    try:
        if not os.path.isfile(sys.argv[1]):
            show_usage()
            return
        label_list_IT, feat_space_IT, w_avg_IT = get_weight('../../model/IT.model.noloc')
        label_list_YOU, feat_space_YOU, w_avg_YOU = get_weight('../../model/YOU.model.noloc')
        label_list_THEY, feat_space_THEY, w_avg_THEY = get_weight('../../model/THEY.model.noloc')
        label_list_LOS, feat_space_LOS, w_avg_LOS = get_weight('../../model/LOS.model.noloc')
        label_list_TO, feat_space_TO, w_avg_TO = get_weight('../../model/TO.model.noloc')
        print('\nModel files loaded.', file=sys.stderr)
    except IndexError:
        show_usage()
        return
    except OSError:
        show_usage()
        return

    # test_sample = features vec derived from test_sample_line
    # result_word = one classified resulting word to be written back to result_line
    # final output is corrected test_sample_line, denoted as result_line
    test_sample_line, templine, result_line = [], [], []
    test_sample, result_word = [], []
    linecount = 0
    with open(sys.argv[1], 'r') as f:
        for line in f:
            linecount += 1
            print('Preprocessing line number:', linecount, file=sys.stderr)            
            if line == '\n':
                print('')
                continue
            test_sample_line = line
            result_line = []
            templine = test_sample_line.lower()

            
            if "it's" or "you're" or "they're" in templine:
                templine = underscore(templine)

            # record location of target homophones
            templist = templine.split()
            target_location = []
            for i, word in enumerate(templist):
                if word == 'its' or word == 'it_s' or word == 'your' or word == 'you_re' or \
                    word == 'their' or word == 'they_re' or word == 'lose' or word == 'loose' or \
                    word == 'to' or word =='too':
                    target_location.append(i)

            if not target_location: 
                # need to strip \n b.c. print() adds \n
                print(test_sample_line.rstrip('\n'))
                continue

            temptokens = nltk.word_tokenize(templine)
            corrections = []
            pos = []
            # scan each word and classify for homophone cases, denoted as test_sample, if found
            # otherwise output original test_sample_line
            for i, token in enumerate(temptokens):
                if  token == 'its' or token == "it_s":
                    if not pos: 
                        pos = postag(temptokens)
                    test_sample = create_feat(i, pos)
                    result_token = classify(test_sample, w_avg_IT, label_list_IT)
                    corrections.append(apostrophe(result_token))

                if  token == 'your' or token == "you_re":
                    if not pos: 
                        pos = postag(temptokens)
                    test_sample = create_feat(i, pos)
                    result_token = classify(test_sample, w_avg_YOU, label_list_YOU)
                    corrections.append(apostrophe(result_token))

                if  token == 'their' or token == "they_re":
                    if not pos: 
                        pos = postag(temptokens)
                    test_sample = create_feat(i, pos)
                    result_token = classify(test_sample, w_avg_THEY, label_list_THEY)
                    corrections.append(apostrophe(result_token))

                if  token == 'lose' or token == "loose":
                    if not pos: 
                        pos = postag(temptokens)
                    test_sample = create_feat(i, pos)
                    result_token = classify(test_sample, w_avg_LOS, label_list_LOS)
                    corrections.append(apostrophe(result_token))

                if  token == 'to' or token == "too":
                    if not pos: 
                        pos = postag(temptokens)
                    test_sample = create_feat(i, pos)
                    result_token = classify(test_sample, w_avg_TO, label_list_TO)
                    corrections.append(apostrophe(result_token))

            # print(corrections, file=sys.stderr)

            # result_line = detokenize(temptokens, test_sample_line)
            result_line = map_back(test_sample_line, target_location, corrections)
            print('\nINPUT:', test_sample_line, file=sys.stderr)
            print('RESULT:',result_line, '\n',file=sys.stderr)
            print(result_line)
            # input()

    print('\nTotal number of lines are:', linecount, file=sys.stderr)


if __name__ == '__main__':
	main()