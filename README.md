CSCI 544 HW3 by Yin-Ray Rick Huang

Homophone grammar correction, saved as hw3.output.txt

External packages used: NLTK, to tokenize and take POS tags

I use perceplearn.py from hw2 to build model files:

-- USAGE: python3 perceplearn.py TRAININGFILE MODELFILE

-- Output MODELFILE in such format:	CLASS	FEATURE	  WEIGHT; 


( Tried 8 and 16 iterations for perceplearn.py. No distinguishable difference.)


And modify percepclassify.py for our current homophone word correction task.

-----------------------------------------------------------------------------------------

Steps to create the training file: preprocess.py

1. Change target words with apostrophe into separated by "_". E.g. "they're" to "they_re". 
   This is to avoid unwanted tokenization by NLTK.

2. Make every character lower case. Use NLTK to tokenize the training data, and perform POS tagging.

3. Obtain features using previous 2 & next 2 POS and previous 2 & next 2 words. 

4. Extra feature: relative target word location within the sentence: 0, 1, 2, ..., 10. "0" is the beginning and "10" is the end of sentence. This can be calculated by [10* (word_index) / length]. Create 5 training files. Each homophone case is handled by a class object.

5. Build 5 binary classifiers to deal with each homophone cases and write to 5 model files. 

USAGE: python3 preprocess.py <raw_text_data_file>

-----------------------------------------------------------------------------------------

Steps to classify the test set: correcterror.py

1. Scan each word, look for target homophone.

2. Preprocess similar to obtaining the training file.

3. Call corresponding models. Five model files hard-coded. This is different from percepclassify.py.

4. Classify and write back. Preserve original test data format.

-- USEAGE: python3 correcterror.py IN_file 

-- Each corrected line is directed to STDOUT

-----------------------------------------------------------------------------------------

Results on the dev set vs original hw3.dev.err.txt baseline:

***********************************************************

ITS or IT'S: Different rate = 0.03498542274052478
Baseline: Different rate = 0.29737609329446063

***********************************************************
 
YOUR or YOU'RE: Different rate = 0.0519159456118665
Baseline: Different rate = 0.315203955500618

***********************************************************
 
THEIR or THEY'RE: Different rate = 0.018756169792694965
Baseline: Different rate = 0.2912142152023692

***********************************************************

LOSE or LOOSE: Different rate = 0.1
Baseline: Different rate = 0.2571428571428571

***********************************************************

TOO or TO: Different rate = 0.02063476466542203
Baseline: Different rate = 0.301021912154859

